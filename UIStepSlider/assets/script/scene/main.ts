import UIStepSlider from "../UIStepSlider/UIStepSlider";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Main extends cc.Component {
    @property(UIStepSlider)
    private stepSlider: UIStepSlider = null;

    @property(cc.Label)
    private lblText: cc.Label = null;

    protected onLoad(): void {
        this.stepSlider.reload(10);
    }

    onSliderChange() {
        const value = this.stepSlider.getProgress();
        this.lblText.string = value.progress + ', ' + value.index;
    }
}
