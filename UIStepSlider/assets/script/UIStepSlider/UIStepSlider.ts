const {ccclass, property, menu, requireComponent} = cc._decorator;

export interface IUIStepSliderProgress {
    /**
     * 进度 0 ~ 1
     */
    progress: number;

    /**
     * 当前进度在数据中的 `index`
     */
    index: number;
}

/**
 * 分步的 slider.  
 * 
 * 会将 slider 分成若干份。当用户结束触摸时，进度一定会规定的百分比上
 */
@ccclass
@menu('UIKit/UIStepSlider')
@requireComponent(cc.Slider)
export default class UIStepSlider extends cc.Component {
    private _timerFix: number = 0;
    private _lastEmitInfo: IUIStepSliderProgress = null;
    private _progresses: number[] = [0, 1];

    @property({type: [cc.Component.EventHandler]})
    slideEvents: cc.Component.EventHandler[] = [];

    static EventStepSlider = 'StepSlider';

    onLoad() {
        
    }

    onDestroy() {
        
    }

    onEnable() {
        this._onSlide();
        this.node.on('slide', this._onSlide, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this._onTouchEnded, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this._onTouchEnded, this);

        const slider = this.getComponent(cc.Slider);
        const handle = slider.handle;
        if (handle && handle.isValid) {
            handle.node.on(cc.Node.EventType.TOUCH_END, this._onTouchEnded, this);
            handle.node.on(cc.Node.EventType.TOUCH_CANCEL, this._onTouchEnded, this);
        }
    }

    onDisable() {
        this.node.off('slide', this._onSlide, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this._onTouchEnded, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this._onTouchEnded, this);

        const slider = this.getComponent(cc.Slider);
        const handle = slider.handle;
        if (handle && handle.isValid) {
            handle.node.off(cc.Node.EventType.TOUCH_END, this._onTouchEnded, this);
            handle.node.off(cc.Node.EventType.TOUCH_CANCEL, this._onTouchEnded, this);
        }
    }


    /**
     * 
     * @param progresses 需要分割成的进度数据，由小到大排行，数值 0 ~ 1。长度>= 2
     */
    reload(progresses: number[]): UIStepSlider

    /**
     * 
     * @param count 需要将进度分割是 `count` 份。必须 >= 2
     */
    reload(count: number): UIStepSlider
    reload(arg: any): UIStepSlider {
        if (Array.isArray(arg)) {
            this._progresses = this.arrayToPregress(arg);
        } else {
            this._progresses = this.countToPregress(arg);
        }

        this._lastEmitInfo = null;

        const value = this.toFix(this.getSliderProgress());

        this.setSliderProgress(value.progress);

        return this;
    }

    setSliderProgress(v: number) {
        const slider = this.getComponent(cc.Slider);
        if (slider) {
            slider.progress = v;
        }
    }

    getSliderProgress() {
        return this.getComponent(cc.Slider)?.progress || 0;
    }

    getProgress() {
        return this._lastEmitInfo || {progress: 0, index: 0};
    }

    private fixProgress() {
        const progres = this._onSlide();
        this.setSliderProgress(progres);
    }

    private toFix(progress: number): IUIStepSliderProgress {
        const count = this._progresses.length;
        for (let index = 0; index < (count - 1); ++index) {
            const p1 = this._progresses[index];
            const p2 = this._progresses[index + 1];

            if (p1 == progress) {
                return {progress, index};
            }

            if (p2 == progress) {
                return {progress, index: index + 1};
            }

            if (p1 < progress && progress < p2) {
                if ((progress - p1) < (p2 - progress)) {
                    return {progress: p1, index};
                } else {
                    return {progress: p2, index: index + 1};
                }
            }
        }

        return {progress: 1.0, index: count - 1};
    }

    private arrayToPregress(numbers: number[]): number[] {
        let set: Set<number> = new Set();

        for (let v of numbers) {
            if (v >= 0 && v <= 1.0) {
                set.add(v);
            } else {
                cc.error(`Invalid prgress: `, v);
            }
        }

        const values = Array.from(set.values()).sort();

        if (values.length == 0) {
            values.push(0, 1);
        }

        if (values[0] > 0.0) {
            values.unshift(0.0);
        }

        if (values[values.length - 1] < 1.0) {
            values.push(1.0);
        }

        return values;
    }

    private countToPregress(count: number): number[] {
        let values: number[] = [0];
        
        if (count < 2) {
            cc.error(`Invalid count: `,  count);
            count = 2;
        }

        let step = 1.0 / (count - 1);
        for (let i = 1; i < count - 1; ++i) {
            values[i] = (step * i);
        }

        values.push(1);

        return values;
    }

    private _onSlide() {
        const slider: cc.Slider = this.getComponent(cc.Slider);
        const progress = slider?.progress;

        if (!slider) {
            return 0;
        }

        const value = this.toFix(progress);
        this._emit(value);

        return value.progress;
    }

    private _emit(info: IUIStepSliderProgress) {
        if (this._lastEmitInfo && (this._lastEmitInfo.progress == info.progress) && (this._lastEmitInfo.index == info.index)) {
            return;
        }

        this._lastEmitInfo = info;

        this.node.emit(UIStepSlider.EventStepSlider, this);

        cc.Component.EventHandler.emitEvents(this.slideEvents, this);
    }

    private _onTouchEnded() {
        if (this._timerFix <= 0) {
            this._timerFix = setTimeout(() => {
                this._timerFix = 0;

                cc.isValid(this) && this.fixProgress();   
            });
        }
    }
}
